// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const googleCloudStorage = require('@google-cloud/storage')();

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

exports.handleDeleteApplication = functions.database.ref('Applications/{app}').onDelete((snap, context) => {
    const deletedData = snap.val()
    const appID = deletedData.appId
    const screenshot = Object.keys(deletedData.screenshots).length
    const developerID = deletedData.developerID
    admin.database().ref(`Users/${developerID}/yourApp/${appID}`).set(null)

    const appIconFilePath = appID+'/appIcon/appIcon.png'
    const bucket = googleCloudStorage.bucket('egginhouseapp-d59c9.appspot.com')
    const file = bucket.file(appIconFilePath)
    
    file.delete().then( () => { 
        console.log("log deleted!")
        return
    }).catch( error => {
        console.log("errorrrrrr", error)
    })

    for (let index = 0; index < screenshot; index++) {
        deleteScreenshotsStorage(index+1, bucket, appID)
    }
    const fileManifestPath = appID+'/ipa/manifest.plist'
    const fileManifest = bucket.file(fileManifestPath)
    fileManifest.delete().then(() => {
        console.log("Deleted Manifest file")
        return
    }).catch((error)=>{
        console.log("error : delete manifest")
    })

    const fileIPAPath = appID+'ipa/app.ipa'
    const fileIPA = bucket.file(fileIPAPath)
    fileIPA.delete().then(value => {
        console.log("deleted IPA file", error)
        return 
    }).catch((error) => {
        console.log("error delete IPA file", error)
    })

})

function deleteScreenshotsStorage(i, bucket, appID) {

    const scrsFilePath = appID+'/screenshots/img'+i+'.png'
    const fileScrs = bucket.file(scrsFilePath)
    console.log("for loop:", i)
    try {
       fileScrs.delete()
       return
    }
    catch (err) {
      console.log("delete err", err.stack)
      return
    }
    

  }


// const rawData = {
//     appIconUrl: 'https://firebasestorage.googleapis.com/v0/b/egginhouseapp-d59c9.appspot.com/o/-L9Df9WC5IlXF8UXYjjo%2FappIcon%2FappIcon.png?alt=media&token=243e343c-977e-48d6-8d33-ec00dc7fcc51',
//     appId: '-L9Df9WC5IlXF8UXYjjo',
//     build: '1.0',
//     bundleId: '1.0',
//     description: '1.0',
//     developerID: 'HmuY1qA6wjOZf5qvniNM9FS8wbw2',
//     device: 'iPhone',
//     download: 1,
//     fileAppUrl: 'https://mhzw9.app.goo.gl/k2XH',
//     lastest_timestamp: 1522812141540,
//     name: 'qwerty',
//     permission: { General: true },
//     screenshots:
//         {
//             img1: 'https://firebasestorage.googleapis.com/v0/b/egginhouseapp-d59c9.appspot.com/o/-L9Df9WC5IlXF8UXYjjo%2Fscreenshots%2Fimg1.png?alt=media&token=5fde6f29-54f7-4708-946f-4e22442c5717',
//             img2: 'https://firebasestorage.googleapis.com/v0/b/egginhouseapp-d59c9.appspot.com/o/-L9Df9WC5IlXF8UXYjjo%2Fscreenshots%2Fimg2.png?alt=media&token=49b1e6d8-28d6-4885-bd7d-fb6e4c7549a0',
//             img3: 'https://firebasestorage.googleapis.com/v0/b/egginhouseapp-d59c9.appspot.com/o/-L9Df9WC5IlXF8UXYjjo%2Fscreenshots%2Fimg3.png?alt=media&token=6d05668c-b69f-4cba-84d3-4f9f24f91f6d',
//             img4: 'https://firebasestorage.googleapis.com/v0/b/egginhouseapp-d59c9.appspot.com/o/-L9Df9WC5IlXF8UXYjjo%2Fscreenshots%2Fimg4.png?alt=media&token=6f2e0113-811e-43d4-8e4d-3edd1b5e1983',
//             img5: 'https://firebasestorage.googleapis.com/v0/b/egginhouseapp-d59c9.appspot.com/o/-L9Df9WC5IlXF8UXYjjo%2Fscreenshots%2Fimg5.png?alt=media&token=8074ce0d-754c-4a66-81ff-66cb8ca6c34d'
//         },
//     timeStamp: 1522812141540,
//     version: '1.0'
// }

/*
const functions = require('firebase-functions')
const admin = require('firebase-admin')

admin.initializeApp(functions.config().firebase)

const googleCloudStorage = require('@google-cloud/storage')()

exports.triggerApplications = functions.database.ref('/Applications/{appId}/group_access').onWrite(event => {
  const access_groups = event.data.val()
  console.log("name", event.params.appId, access_groups)
  const idApp = event.params.appId
  var db = admin.database()

  for (val in access_groups) {
    console.log("log access group: ", val)
    var ref = db.ref("Groups").child(val).child("app_permission").child(idApp)
     ref.set(true)
  }

});

exports.handleDeleteApplication = functions.database.ref('Groups/{groupIDs}/app_permission').onDelete(event => {
  const listApp = event.data.previous.val()
  const groupId = event.params.groupIDs
  console.log("app permission: ", listApp) //OUTPUT => { '7MheeYai': true, MheeYai: true }~~~
  console.log("groupIDs: ", groupId) //OUTPUT => dev23
  for (var appId in listApp) {
    admin.database().ref('Applications/'+appId+'group_access/'+groupId).set(null)
  }

})


exports.handleDeleteGroups = functions.database.ref('Groups/{groupId}/members').onDelete(event => {
  //const usersUIds = event.data.val()
  const groupId = event.params.groupId //dev3
  const parentKeyUid = event.data.ref.parent.key //dev3

  const userUids = event.data.previous.val()
  console.log("data: ", userUids)
  
  var db = admin.database()

  for (var key in userUids){
    console.log("delete Node") 
    var ref = admin.database().ref('Users/'+key+'/groups/'+groupId)
    ref.set(null)
  }
  
})


exports.testDeleteFileFromUrl = functions.database.ref('test/').onWrite(event => {

})

exports.handleDeleteApp = functions.database.ref('Applications/{appId}').onDelete(event => {

  let appId = event.params.appId
  let valData = event.data.val()

  let previousData = event.data.previous.val()
  

  console.log('appId: ', appId)
  console.log('valData: ', valData)
  console.log('previousData: ', previousData)

        const nameApp = previousData["appId"]
        const devID = previousData["developerID"]
        console.log('appID: ', nameApp)
  
  		admin.database().ref("Users/"+devID+"/yourApp/"+nameApp).set(null)
	
        //const groupAccess = previousData["access_group"]
        //for (group in groupAccess) {
          return admin.database.ref("Groups/"+group+"/app_permission/"+appId).set(null)
        //}

        const screenshotObj = previousData["screenshots"]
        console.log('obj screenshot: ', screenshotObj)

        const screentshotCount = Object.keys(screenshotObj).length
        console.log("count: ", screentshotCount)

        const appIconFilePath = nameApp+'/appIcon/appIcon.png'
        const bucket = googleCloudStorage.bucket('egginhouseapp-d59c9.appspot.com')
        const file = bucket.file(appIconFilePath)
        file.delete().then(function (value) {
          //throw "delete complete"
          console.log("deleted app icon")
          return value
        }).catch((error) => {
          console.log("error app icon: ", error)
        })

        for (var i = 0; i < screentshotCount; i++ ){
          deleteImgStorage(i+1, bucket, nameApp)

        }
  		const fileIpaPath = nameApp+'/ipa/app.ipa'
        const fileScrr = bucket.file(fileIpaPath)
        try {
        	fileScrr.delete()
        }
		catch (err) {
        	console.log(err.stack)
        }
    	const fileManifestPath = nameApp+'/ipa/manifest.plist'
        const fileScrrs = bucket.file(fileManifestPath)
        try {
        	fileScrrs.delete()
        }
		catch (err) {
        	console.log(err.stack)
        }
  
  
        console.log("WoHooo! delete complete!!!!!!!")
    })

   function deleteImgStorage(i, bucket, nameApp) {

      const scrsFilePath = nameApp+'/screenshots/img'+i+'.png'
      const fileScrs = bucket.file(scrsFilePath)
      console.log("for loop:", i)
      try {
         fileScrs.delete()
         return
      }
      catch (err) {
        console.log(err.stack)

        return
      }
      

    }



    exports.hello = functions.database.ref('/hello').onWrite(event => {
      return event.data.ref.set('world!').then(() => {
        console.log('Write succeeded!');
        return
      });
    });
    */