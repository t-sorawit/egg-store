//
//  ApplicationsItemTableViewCell.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/24/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit

class ApplicationItemTableViewCell: UITableViewCell {

    @IBOutlet weak var appIconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        appIconView.layer.cornerRadius = appIconView.frame.height / 4
        appIconView.clipsToBounds = true
        nameLabel.textColor = UIColor.flat.midnightblue
    }
}
