//
//  AddNewApplicationViewController.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Photos
import BSImagePicker
import Firebase
import PKHUD
import Kingfisher
import Alamofire
import Alamofire
import FirebaseDynamicLinks

class AddNewApplicationViewController: BaseViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var appIconView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var versionTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var buildTextField: UITextField!
    @IBOutlet weak var bundleIdTextField: UITextField!
    @IBOutlet weak var appFileUrlTextField: UITextField!
    @IBOutlet weak var screenshotCollectionView: UICollectionView!
    @IBOutlet weak var editAccess: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet var devices: [UIButton]!
    var radioButtonsController: RadioButtonsController?
    var indicator: UIActivityIndicatorView!
    var selectedAssests = [PHAsset]()
    var screenshotArray = [UIImage]()
    var application: ApplicationModel!
    var isUpdate = false
    var groupNameAccess: [String] = ["General"]
    var selectedButton = ""
    let inviteGroupVC = InviteUserViewController()
    var stringUrlScreenshot: [URL] = [URL]()
    @IBOutlet weak var screenshotCollectionViewHeight: NSLayoutConstraint!
    var delegateInvite: InviteUserViewControllerDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        radioButtonsController = RadioButtonsController(buttons: devices[0], devices[1], devices[2])
        radioButtonsController!.delegate = self
        radioButtonsController!.shouldLetDeSelect = true
        setupNavigationBar()
        setupImagePicker()
        setupCollectionView()
        setupTextField()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        inviteGroupVC.delegateVC = self
    }
    func setupTextField() {
        nameTextField.setBottomBorder(width: 1, color: UIColor.Flat.grayBrown.cgColor)
        versionTextField.setBottomBorder(width: 1, color: UIColor.Flat.grayBrown.cgColor)
        buildTextField.setBottomBorder(width: 1, color: UIColor.Flat.grayBrown.cgColor)
        bundleIdTextField.setBottomBorder(width: 1, color: UIColor.Flat.grayBrown.cgColor)
        appFileUrlTextField.setBottomBorder(width: 1, color: UIColor.Flat.grayBrown.cgColor)
    }
    func setupView() {
        appIconView.layer.cornerRadius = appIconView.frame.height / 4
        appIconView.clipsToBounds = true
        editAccess.backgroundColor = .clear
        editAccess.layer.cornerRadius = 2.0
        editAccess.layer.borderColor = UIColor.Yellow.egg.cgColor
        editAccess.layer.borderWidth = 1.0
        editAccess.addTarget(self, action: #selector(handleAddGroup), for: .touchUpInside)
        saveBtn.addTarget(self, action: #selector(handleSaveApps), for: .touchUpInside)
        saveBtn.layer.cornerRadius = 2.0
        devices[0].setTitle("iPhone", for: .normal)
        devices[0].tintColor = UIColor.Yellow.egg
        devices[1].setTitle("iPad", for: .normal)
        devices[1].tintColor = UIColor.Yellow.egg
        devices[2].setTitle("Universal", for: .normal)
        devices[2].tintColor = UIColor.Yellow.egg
        if let application = application {
            switch application.device.rawValue {
            case (devices[0].titleLabel?.text)!: radioButtonsController?.pressed(devices[0])
            case (devices[1].titleLabel?.text)!: radioButtonsController?.pressed(devices[1])
            case (devices[2].titleLabel?.text)!: radioButtonsController?.pressed(devices[2])
            default:
                radioButtonsController?.pressed(devices[2])
            }
            isUpdate = true
            nameTextField.text = application.name
            versionTextField.text = application.version
            bundleIdTextField.text = application.bundleId
            buildTextField.text = application.build
            let strUrl = application.appIconUrl
            let url = URL(string: strUrl)
            appIconView.kf.setImage(with: url)
            descriptionTextView.text = application.description
            appFileUrlTextField.text = application.fileAppUrl
            appFileUrlTextField.isUserInteractionEnabled = false
            for item in application.screenshots {
                let url = URL(string: item.value)
                stringUrlScreenshot.append(url!)
                screenshotCollectionViewHeight.constant = 375
            }
        }
    }
    func setupImagePicker() {
        let screenshotTap = UITapGestureRecognizer(target: self, action: #selector(handleToPicker))
        screenshotTap.delegate = self
        screenshotTap.numberOfTapsRequired = 1
        let appIconTap = UITapGestureRecognizer(target: self, action: #selector(handleSelectAppIconView))
        appIconTap.delegate = self
        appIconTap.numberOfTapsRequired = 1
        appIconView.addGestureRecognizer(appIconTap)
        screenshotCollectionView.isUserInteractionEnabled = true
        if isUpdate {
            appIconView.isUserInteractionEnabled = false
        } else {
            appIconView.isUserInteractionEnabled = true
            screenshotCollectionView.addGestureRecognizer(screenshotTap)
        }
    }
    @objc func handleAddGroup() {
        let inviteGroup = InviteUserViewController()
        inviteGroup.delegateVC = self
        self.navigationController?.pushViewController(inviteGroup, animated: true)
    }
    func setupNavigationBar() {
        self.navigationItem.title = "New Application"
    }
    func setupCollectionView() {
        let nibCell = UINib(nibName: "AppDetailCollectionViewCell", bundle: nil)
        self.screenshotCollectionView.register(nibCell, forCellWithReuseIdentifier: "AppDetailCollectionViewCell")
        self.screenshotCollectionView.delegate = self
        self.screenshotCollectionView.dataSource = self
    }

    @objc func handleSaveApps() {
        if (nameTextField.text?.isEmpty)! || (versionTextField.text?.isEmpty)! || (descriptionTextView.text?.isEmpty)! || (buildTextField.text?.isEmpty)! || (bundleIdTextField.text?.isEmpty)! || (appFileUrlTextField.text?.isEmpty)! {
            showAlertMsg(withViewController: self, title: "invalid", message: "Please enter a textFields")
            return
        }
        if isUpdate {
            setApplicationData(refKey: "null", screenshotUrls: [""], iconUrl: "")
            self.navigationController?.popViewController(animated: true)
        } else {
            let refKey = Database.database().reference(withPath: "Applications").childByAutoId().key
            if screenshotArray.count ==  0 {
                showAlertMsg(withViewController: self, title: "Invalid", message: "please input screenshot")
                return
            }
            guard let appIcon = appIconView.image else {
                showAlertMsg(withViewController: self, title: "Invalid", message: "please input icon")
                return
            }
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show()
            Firebase.uploadImageFile(appName: refKey, appIcon: appIcon, screenshots: screenshotArray) { [weak self] (hasFinished, iconUrl, screenshotsUrl) in
                if hasFinished {
                    self?.setApplicationData(refKey: refKey, screenshotUrls: screenshotsUrl, iconUrl: iconUrl)
                    PKHUD.sharedHUD.hide(afterDelay: 1, completion: { (_) in
                        self?.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }
    func getShortURLFromLongURL(longUrl: String, completion: @escaping(String) -> Void ) {
        guard let longURL = URL(string: longUrl) else { return }
        let domainUrl = "mhzw9.app.goo.gl"
        let componentDynamicLink = DynamicLinkComponents(link: longURL, domain: domainUrl )
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        componentDynamicLink.options = options
        componentDynamicLink.shorten { (url, _, error) in
            if error != nil {
                print("error dynamic link")
            } else {
                completion("\(url!)")
            }
        }
    }
    func setApplicationData(refKey: String, screenshotUrls: [String], iconUrl: String) {
        guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return }
        guard let name = nameTextField.text else { return }
        guard let version = versionTextField.text else { return }
        guard let build = buildTextField.text else { return }
        guard let bundleId = buildTextField.text else { return }
        guard let description = descriptionTextView.text else { return }
        guard let fileAppUrl = appFileUrlTextField.text else { return }
            var countImg = 1
            var screenDict = [String: Any]()
            for screenshotUrl in screenshotUrls {
                screenDict["img\(countImg)"] = screenshotUrl
                countImg += 1
            }
            var groupsAccess = [String: Any]()
            for groupName in groupNameAccess {
                groupsAccess["\(groupName)"] = true
            }
            let timestamp = ServerValue.timestamp()
            let ref = Database.database().reference().child("Applications").child(refKey)
            var manifestDictionary: NSMutableDictionary?
            let pathManifest = Bundle.main.path(forResource: "manifest", ofType: "plist")
            manifestDictionary = NSMutableDictionary(contentsOfFile: pathManifest!)
            var urlDownloadIpa = fileAppUrl
            var shortURL = "shortURL"
            if let dict = manifestDictionary {
                let childPath = dict.mutableArrayValue(forKey: "items")
                childPath.mutableArrayValue(forKey: "assets").setValue(fileAppUrl, forKey: "url")
                childPath.mutableArrayValue(forKey: "metadata").setValue(bundleId, forKey: "bundle-identifier")
                childPath.mutableArrayValue(forKey: "metadata").setValue(name, forKey: "title")
                childPath.write(toFile: pathManifest!, atomically: true)
                do {
                    let data = try PropertyListSerialization.data(fromPropertyList: manifestDictionary, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
                    var applicationID = ""
                    if isUpdate {
                        applicationID = application.appId
                    } else {
                        applicationID = refKey
                    }
                    let manifestRef = Storage.storage().reference().child(applicationID).child("ipa/manifest.plist")
                    manifestRef.putData(data, metadata: nil, completion: { (storage, _) in
                        let url = storage?.downloadURL()
                        urlDownloadIpa = "\(url!)"
                        self.getShortURLFromLongURL(longUrl: urlDownloadIpa, completion: { (shortenURL) in
                            shortURL = shortenURL
                            if self.isUpdate {
                                let appData = ["name": name, "version": version, "build": build, "bundleId": bundleId, "description": description, "permission": groupsAccess, "lastest_timestamp": timestamp, "fileAppUrl": shortURL, "device": self.selectedButton] as [String : Any]
                                let ref = Database.database().reference().child("Applications").child(self.application.appId)
                                ref.updateChildValues(appData)
                            } else {
                                let appData = ["name": name, "appId": refKey, "developerID": "\(user.uid)", "version": version, "build": build, "bundleId": bundleId, "screenshots": screenDict, "description": description, "fileAppUrl": shortURL, "appIconUrl": iconUrl, "permission": groupsAccess, "download": 0, "timeStamp": timestamp, "lastest_timestamp": timestamp, "device": self.selectedButton] as [String: Any]
                                ref.setValue(appData)
                                Database.database().reference(withPath: "Users").child("\(user.uid)/yourApp").updateChildValues([refKey: true])
                            }
                        })
                    })
                } catch let error {
                    print("error", error)
                }
            }
    }
    @objc func handleToPicker() {
        selectedAssests.removeAll()
        let screenshotVC = BSImagePickerViewController()
        self.bs_presentImagePickerController(screenshotVC, animated: true,
                                             select: { (assets: PHAsset) -> Void in
        }, deselect: { (assets: PHAsset) -> Void in
        }, cancel: { (assets: [PHAsset]) -> Void in
        }, finish: { (assets: [PHAsset]) in
            for asset in assets {
                self.selectedAssests.append(asset)
            }
            self.convertAssetsToImages()
        }, completion: nil)
    }
    func convertAssetsToImages() {
        screenshotArray.removeAll()
        if !selectedAssests.isEmpty {
            for asset in selectedAssests {
                let manager = PHImageManager.default()
                let options = PHImageRequestOptions()
                var tumbnail = UIImage()
                let size = CGSize(width: 200, height: 200)
                options.isSynchronous = true
                manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFill, options: options, resultHandler: { (image, _) in
                    tumbnail = image!
                })
                let imgData = UIImageJPEGRepresentation(tumbnail, 0.7)
                let newImage  = UIImage(data: imgData!)
                self.screenshotArray.append(newImage!)
                DispatchQueue.main.async {
                    self.screenshotCollectionViewHeight.constant = 375
                    self.screenshotCollectionView.reloadData()
                }
            }
        }
    }
}

extension AddNewApplicationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func handleSelectAppIconView() {
        let picker = UIImagePickerController()
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        var originalImage: UIImage?
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            originalImage = image
        }
        if let selectedImage = originalImage {
            appIconView.image  = selectedImage
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddNewApplicationViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isUpdate {
            return stringUrlScreenshot.count
        } else {
            return screenshotArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppDetailCollectionViewCell", for: indexPath) as! AppDetailCollectionViewCell
        if isUpdate {
            let url = stringUrlScreenshot[indexPath.row]
            cell.screenshotView.kf.setImage(with: url)
        } else {
            cell.screenshotView.image = screenshotArray[indexPath.row]
        }
       return cell
    }
}

extension AddNewApplicationViewController: InviteUserViewControllerDelegate {
    func didFinishSelectUsers(uses: [String]) {
        groupNameAccess = uses
    }
}

extension AddNewApplicationViewController: RadioButtonControllerDelegate {
    func didSelectButton(selectedButton: UIButton?) {
        guard let selected = selectedButton?.titleLabel?.text else { return }
        self.selectedButton = selected
    }
}
