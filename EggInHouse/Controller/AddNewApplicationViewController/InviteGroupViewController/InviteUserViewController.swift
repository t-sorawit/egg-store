//
//  InviteGroupViewController.swift
//  EggInHouse
//
//  Created by Egg Digital on 1/2/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Firebase
import PKHUD
import CodableFirebase

protocol InviteUserViewControllerDelegate: class {
    func didFinishSelectUsers(uses: [String])
}

class InviteUserViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var isEdit: Bool = false
    var appID: String = ""
    var users: [UserModel] = []
    var uidPermission: [String] = []
    var userSelected: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        fetchUsers()
        navigationItem.title = "Invite Permission"
    }
    var selectAllBtn: UIBarButtonItem!
    func setupTableView() {
        let nib = UINib(nibName: "ListUsersTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ListUsersTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        setNavigationItem(selectTitle: "Select All")
    }
    func setNavigationItem(selectTitle: String) {
        selectAllBtn = UIBarButtonItem(title: selectTitle, style: .plain, target: self, action: #selector(handleSelectAll))
        let moreBtn = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(handleShowActionSheet))
        self.navigationItem.rightBarButtonItems = [moreBtn, selectAllBtn]
    }
    @objc func handleShowActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let saveAction = UIAlertAction(title: "Save", style: .default) { (_) in
            self.handleSelect()
        }
        let setPublicAction = UIAlertAction(title: "Public", style: .default) { (_) in
            self.handlePublic()
        }
        let setPrivateAction = UIAlertAction(title: "Private", style: .default) { (_) in
            self.hadleOnlyMe()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (_) in
        }
        actionSheet.isSpringLoaded = true
        actionSheet.addAction(saveAction)
        actionSheet.addAction(setPublicAction)
        actionSheet.addAction(setPrivateAction)
        actionSheet.addAction(cancelAction)
        self.navigationController?.present(actionSheet, animated: true, completion: nil)
    }
    weak var delegateVC: InviteUserViewControllerDelegate?
    var toggleSelectAll = false
    @objc func handleSelectAll() {
        let totalRows = tableView.numberOfRows(inSection: 0)
        toggleSelectAll = !toggleSelectAll
        if toggleSelectAll {
            setNavigationItem(selectTitle: "Deselect All")
            userSelected = []
            for row in 0..<totalRows {
                userSelected.append(self.users[row].uid)
                let indexPath = IndexPath(row: row, section: 0)
                if let cell = tableView.cellForRow(at: indexPath) {
                    cell.accessoryType = .checkmark
                }
            }
        } else {
            setNavigationItem(selectTitle: "Select All")
            userSelected = []
            for row in 0..<totalRows {
                let indexPath = IndexPath(row: row, section: 0)
                if let cell = tableView.cellForRow(at: indexPath) {
                    cell.accessoryType = .none
                }
            }
        }
    }
    func handleSelect() {
        if isEdit {
            editPermission(userSelect: userSelected)
            self.navigationController?.popViewController(animated: true)
        } else {
            handleFinish(userSelect: userSelected)
        }
    }
    func handlePublic() {
        if isEdit {
            editPermission(userSelect: ["General"])
            self.navigationController?.popViewController(animated: true)
        } else {
            handleFinish(userSelect: ["General"])
        }
    }
    func hadleOnlyMe() {
        guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return }
        if isEdit {
            editPermission(userSelect: [user.uid])
            self.navigationController?.popViewController(animated: true)
        } else {
            handleFinish(userSelect: [user.uid])
        }
    }
    func handleFinish(userSelect: [String]) {
        delegateVC?.didFinishSelectUsers(uses: userSelect)
        self.navigationController?.popViewController(animated: true)
    }
    func editPermission(userSelect: [String]) {
        print("edit permission pressed")
        var userAccess = [String: Any]()
        for userID in userSelect {
            userAccess[userID] = true
        }
        let refApplications = Database.database().reference().child("Applications/\(appID)/permission")
        refApplications.setValue(userAccess) { (error, _) in
            if let error = error {
                PKHUD.sharedHUD.contentView = PKHUDErrorView(title: "Error", subtitle: "\(error)")
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    func fetchUsers() {
        Firebase.users { (users) in
            self.users = users.filter({ (user) -> Bool in
                return user.uid != Firebase.getCurrentUid()
            })
            print("user", self.users)
            self.tableView.reloadData()
        }
    }
}

extension InviteUserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListUsersTableViewCell", for: indexPath) as! ListUsersTableViewCell
        let user = users[indexPath.row]
        let urlProfileImage = URL(string: user.avatar)
        if uidPermission.index(of: user.uid) != nil {
            cell.accessoryType = .checkmark
            userSelected.append(user.uid)
        }
        cell.nameLabel.text = "\(user.name)"
        cell.emailLabel.text = "\(user.email)"
        cell.profileImgView.kf.setImage(with: urlProfileImage)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath as IndexPath) else { return }
        let userID = users[indexPath.row].uid
        if cell.accessoryType == .checkmark {
            cell.accessoryType = .none
            if let indexUser = userSelected.index(of: userID) {
                userSelected.remove(at: indexUser)
            }
        } else {
            cell.accessoryType = .checkmark
            userSelected.append(userID)
        }
    }
}
