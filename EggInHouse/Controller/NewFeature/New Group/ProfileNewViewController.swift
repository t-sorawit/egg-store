//
//  ProfileNewViewController.swift
//  EggInHouse
//
//  Created by Kong on 5/4/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit

class ProfileNewViewController: UIViewController {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var blurFXBG: UIVisualEffectView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNavigationBar()
    }
    func setupView() {
        avatar.layer.cornerRadius = avatar.frame.width / 2.0
        avatar.layer.shadowRadius = 10
        avatar.clipsToBounds = true
        avatar.layer.shadowOffset = CGSize(width: 0, height: 10)
        avatar.layer.shadowColor = UIColor.black.cgColor
        avatar.layer.shadowOpacity = 0.1
        blurFXBG.layer.cornerRadius = 12
        blurFXBG.layer.masksToBounds = true
        blurFXBG.layer.shadowColor = UIColor.black.cgColor
        blurFXBG.layer.shadowOpacity = 0.2
        blurFXBG.layer.shadowOffset = CGSize(width: -20, height: 30)
        blurFXBG.layer.shadowRadius = 30
    }
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
    }
}
