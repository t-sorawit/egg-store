//
//  ApplicationHCollectionViewCell.swift
//  EggInHouse
//
//  Created by Kong on 11/4/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit

class ApplicationHCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var appTitle: UILabel!
    @IBOutlet weak var appVersion: UILabel!
    @IBOutlet weak var appIconPreview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setCellDetail(applicationModel: ApplicationModel) {
        let iconUrl = URL(string: applicationModel.appIconUrl)
        iconView.kf.setImage(with: iconUrl)
        appTitle.text = applicationModel.name
        appVersion.text = applicationModel.version
        appIconPreview.kf.setImage(with: iconUrl)
    }

}
