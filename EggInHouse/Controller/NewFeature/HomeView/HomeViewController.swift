//
//  HomeViewController.swift
//  EggInHouse
//
//  Created by Kong on 11/4/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class HomeViewController: UIViewController {
    @IBOutlet weak var recommededCollectionView: UICollectionView!
    @IBOutlet weak var latestCollectionView: UICollectionView!
    @IBOutlet weak var previewBG: UIImageView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    var recommededApp: [ApplicationModel] = [] {
        didSet {
            self.recommededCollectionView.reloadData()
        }
    }
    var latestApp: [ApplicationModel] = [] {
        didSet {
            self.latestCollectionView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return }
        let url = URL(string: user.avatar)
        previewBG.kf.setImage(with: url!)
        avatarImage.kf.setImage(with: url!)
        avatarImage.clipsToBounds = true
        avatarImage.layer.cornerRadius = avatarImage.frame.height / 6
        avatarImage.layer.shadowColor = UIColor.lightGray.cgColor
        avatarImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        avatarImage.layer.shadowRadius = 2.0
        avatarImage.layer.shadowOpacity = 1.0
        avatarImage.layer.shadowPath = UIBezierPath(roundedRect: avatarImage.bounds, cornerRadius: avatarImage.layer.cornerRadius).cgPath
        logoutBtn.addTarget(self, action: #selector(handleSignOut), for: .touchUpInside)
        let today = Date()
        let month = Calendar.current.component(.month, from: today)
        let date = Calendar.current.component(.day, from: today)
        dateLabel.textColor = .white
        dateLabel.text = "\(Calendar.current.monthSymbols[month-1]), \(date)"
        usernameLabel.text = user.name
        emailLabel.text = "Email: \(user.email)"
        levelLabel.text = "Level: \(user.level.rawValue)"
        fetchData()
        setupCollectionView()
    }
    @objc func handleSignOut() {
        let alert = UIAlertController(title: "Sign Out", message: "Are you sure?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Sign Out", style: .default, handler: { (_) in
            do {
                try Auth.auth().signOut()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = LoginViewController()
            } catch let error {
                print("Error trying to sign out of Firebase: \(error.localizedDescription)")
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    func fetchData() {
        Firebase.getApplications(byChild: "download") { [weak self] (applications) in
            self?.recommededApp = applications
        }
        Firebase.getApplications(byChild: "timeStamp") { [weak self] (applications) in
            self?.latestApp = applications
        }
    }
    func setupCollectionView() {
        let nibCell = UINib(nibName: "ApplicationHCollectionViewCell", bundle: nil)
        recommededCollectionView.register(nibCell, forCellWithReuseIdentifier: "ApplicationHCollectionViewCell")
        recommededCollectionView.dataSource = self
        recommededCollectionView.delegate = self
        let hNibCell = UINib(nibName: "ApplicationHCollectionViewCell", bundle: nil)
        latestCollectionView.register(hNibCell, forCellWithReuseIdentifier: "ApplicationHCollectionViewCell")
        latestCollectionView.dataSource = self
        latestCollectionView.delegate = self
    }

}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == recommededCollectionView {
            return recommededApp.count
        } else {
            return latestApp.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var application: ApplicationModel!
        if collectionView == recommededCollectionView {
            application = recommededApp[indexPath.row]
        } else {
            application = latestApp[indexPath.row]
        }
        let appDetailVC = AppDetailViewController(name: application.appId)
        self.tabBarController?.navigationController?.pushViewController(appDetailVC, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == recommededCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ApplicationHCollectionViewCell", for: indexPath) as! ApplicationHCollectionViewCell
            let applicationModel = recommededApp[indexPath.row]
            cell.setCellDetail(applicationModel: applicationModel)
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ApplicationHCollectionViewCell", for: indexPath) as! ApplicationHCollectionViewCell
            let applicationModel = latestApp[indexPath.row]
            cell.setCellDetail(applicationModel: applicationModel)
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellHeight = collectionView.bounds.size.height
        return CGSize(width: cellHeight, height: cellHeight)
    }
}
