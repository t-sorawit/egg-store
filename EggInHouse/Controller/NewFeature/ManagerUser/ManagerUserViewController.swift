//
//  ManagerUserViewController.swift
//  EggInHouse
//
//  Created by Kong on 19/4/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Firebase

class ManagerUserViewController: BaseViewController {

    @IBOutlet weak var userTableView: UITableView!
    var users: [UserModel] = [UserModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupNavigationBar()
        fetchUser()
    }
    func setupNavigationBar() {
        self.navigationItem.title = "Manager Users"
    }
    func setupTableView() {
        let nibCell = UINib(nibName: "UserTableViewCell", bundle: nil)
        userTableView.register(nibCell, forCellReuseIdentifier: "UserTableViewCell")
        userTableView.dataSource = self
        userTableView.delegate = self
    }
    func fetchUser() {
        Firebase.users { (users) in
            self.users = users
            self.userTableView.reloadData()
        }
    }
}

extension ManagerUserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        let user = users[indexPath.row]
        cell.setCellData(user: user)
        cell.user = user
        cell.cellDelegate = self
        return cell
    }
    func changeLevel(uid: String, level: String) {
        Database.database().reference(withPath: "Users").child(uid).child("level").setValue(level)
    }
}

extension ManagerUserViewController: UserCellDelegate {
    func didPressButton(user: UserModel) {
        let alertModal = UIAlertController(title: "Email: \(user.email)", message: "Level: \(user.level)", preferredStyle: .actionSheet)
        let superAdminAction = UIAlertAction(title: "Super Admin", style: .default) { (_) in
            self.changeLevel(uid: user.uid, level: "super-admin")
        }
        let developerAction = UIAlertAction(title: "Developer", style: .default) { (_) in
            self.changeLevel(uid: user.uid, level: "developer")
        }
        let userAction = UIAlertAction(title: "User", style: .default) { (_) in
            self.changeLevel(uid: user.uid, level: "user")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertModal.addAction(superAdminAction)
        alertModal.addAction(developerAction)
        alertModal.addAction(userAction)
        alertModal.addAction(cancelAction)
        present(alertModal, animated: true, completion: nil)
    }
}
