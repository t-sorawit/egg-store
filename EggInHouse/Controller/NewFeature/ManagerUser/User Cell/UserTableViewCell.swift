//
//  UserTableViewCell.swift
//  EggInHouse
//
//  Created by Kong on 19/4/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit

protocol UserCellDelegate: class {
    func didPressButton(user: UserModel)
}

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var popoverBtn: UIButton!
    var user: UserModel?
    weak var cellDelegate: UserCellDelegate?
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        avatarImg.layer.cornerRadius = avatarImg.frame.size.height / 2
        avatarImg.clipsToBounds = true
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        popoverBtn.addTarget(self, action: #selector(didPressedButton), for: .touchUpInside)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setCellData(user: UserModel) {
        let url = URL(string: user.avatar)
        avatarImg.kf.setImage(with: url)
        usernameLabel.text = user.name
        emailLabel.text = user.email
        levelLabel.text = user.level.rawValue
    }
    @objc func didPressedButton() {
        cellDelegate?.didPressButton(user: user!)
    }
}
