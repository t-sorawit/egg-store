//
//  ListOurApplicationsViewController.swift
//  EggInHouse
//
//  Created by Kong on 3/21/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Firebase
import PKHUD

class ListOurApplicationsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var applicationsList: [ApplicationModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupNavigation()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchOurApplication()
    }
    func setupTableView() {
        let nib = UINib(nibName: "ListApplicationsTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ApplicationMiniCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
    }
    func setupNavigation() {
        self.navigationItem.title = "Our Applications"
        let addAppItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddNewApp))
        self.navigationItem.rightBarButtonItem = addAppItem
    }
    @objc func handleAddNewApp() {
        let addNewAppVC = AddNewApplicationViewController()
        self.tabBarController?.navigationController?.pushViewController(addNewAppVC, animated: true)
    }
    func fetchOurApplication() {
        let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER")
        Firebase.fetchOurApplications(uid: (user?.uid)!) { (applications) in
            self.applicationsList = applications
        }
    }
}

extension ListOurApplicationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return applicationsList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApplicationMiniCell", for: indexPath) as! ListApplicationsTableViewCell
        let applicationItem = applicationsList[indexPath.row]
        cell.listApplicationsTableViewCellDelegate = self
        cell.tag = indexPath.row
        cell.indexP = indexPath
        cell.setData(applicationModel: applicationItem)
        return cell
    }
}

extension ListOurApplicationsViewController: UITableViewDelegate, ListApplicationsViewCellDelegate {
    func didHandleInstall(indexPath: IndexPath) {
        let fileUrl = applicationsList[indexPath.row].fileAppUrl
        let appId = applicationsList[indexPath.row].appId
        let path = "itms-services://?action=download-manifest&url="
        let url = URL(string: path+fileUrl)
        if UIApplication.shared.canOpenURL(url!) {
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show()
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
        let refDownload = Database.database().reference().child("Applications/\(appId)/download")
        refDownload.observeSingleEvent(of: .value, with: { (datasnapshot) in
            guard let downloadRaw = datasnapshot.value else { return }
            let download = downloadRaw as! Int
            refDownload.setValue(download + 1)
        })
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let application = applicationsList[indexPath.row]
        let appDetailVC = AppDetailViewController(name: application.appId)
        self.tabBarController?.navigationController?.pushViewController(appDetailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let application = self.applicationsList[indexPath.row]
        guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return nil }
        let editPermissionAction  = UITableViewRowAction(style: .default, title: "Edit Permission") { (_, _) in
            let alert = UIAlertController(title: "Remove Permission", message: "Are you sure?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                let editPermissionVC = InviteUserViewController()
                editPermissionVC.isEdit = true
                editPermissionVC.appID = application.appId
                editPermissionVC.uidPermission = application.permission.allKeys(forValue: true)
                self.tabBarController?.navigationController?.pushViewController(editPermissionVC, animated: true)
            })
            let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        editPermissionAction.backgroundColor = UIColor.Flat.blue
        let editAppsAction = UITableViewRowAction(style: .default, title: "Edit") { (_, _) in
            let editVC = AddNewApplicationViewController()
            editVC.application = application
            self.tabBarController?.navigationController?.pushViewController(editVC, animated: true)
        }
        editAppsAction.backgroundColor = UIColor.Flat.yellow
        let deleteAppsAction = UITableViewRowAction(style: .default, title: "Delete") { (_, _) in
            let alert = UIAlertController(title: "Delete Application", message: "Are you sure?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.handleDeleteApplicationItem(appID: application.appId, userID: user.uid)
            })
            let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
        deleteAppsAction.backgroundColor = UIColor.Flat.red
        return [deleteAppsAction, editAppsAction, editPermissionAction]
    }
    func handleDeleteApplicationItem(appID: String, userID: String) {
        let applicationRef = Database.database().reference(withPath: "Applications")
        applicationRef.child(appID).removeValue()
        let usersRef = Database.database().reference(withPath: "Users")
        usersRef.child(userID).child("yourApp").child(appID).removeValue()
    }
}
