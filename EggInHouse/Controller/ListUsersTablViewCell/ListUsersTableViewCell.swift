//
//  ListUsersTableViewCell.swift
//  EggInHouse
//
//  Created by Kong on 3/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit

class ListUsersTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImgView.layer.cornerRadius = profileImgView.frame.height/2
        profileImgView.clipsToBounds = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
