//
//  ManageGroupViewController.swift
//  EggInHouse
//
//  Created by Egg Digital on 26/1/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase
import Kingfisher

class ManageGroupViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var users: [UserModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    var nameGroup = ""
    var members: [String] = []
    var selectedUsers: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchUsers()
    }
    func fetchUsers() {
        let ref = Database.database().reference().child("Groups").child(nameGroup).child("members")
        ref.observe(.value) { (snapshot) in
            print(snapshot)
            self.users = [UserModel]()
            guard let members = snapshot.value as? [String: Any] else { return }
            self.users.removeAll()

            for uid in members.keys {
                let ref = Database.database().reference().child("Users")
                ref.child(uid).observeSingleEvent(of: .value, with: { (dataSnapshot) in
                    let user = UserModel(snapshot: dataSnapshot)
                    self.users.append(user!)
                })
            }
        }
    }
    func setupTableView() {
        let nib = UINib(nibName: "UsersTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "UsersCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    func setupNavigationBar() {
        let addGroup = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddGroup))
        let removeUser = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(handleRemoveUsers))
        if selectedUsers.isEmpty {
            self.navigationItem.rightBarButtonItems = [addGroup]
        } else {
            self.navigationItem.rightBarButtonItems = [addGroup, removeUser]
        }
        self.navigationItem.title = nameGroup
    }
    func deleteUsers(nameGroup: String, users: [String]) {
        for uid in users {
            let ref = Database.database().reference().child("Groups").child(nameGroup).child("members")
            ref.child(uid).removeValue(completionBlock: { (error, _) in
                if let error = error {
                    print(error)
                } else {
                    print("delete complete!")}
            })
        }
        self.navigationController?.popViewController(animated: true)
    }
    @objc func handleRemoveUsers() {
        let alert = UIAlertController(title: "Remove Permission", message: "Are you sure?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes, I'm.", style: .default, handler: { (_) in
            self.deleteUsers(nameGroup: self.nameGroup, users: self.selectedUsers)
        })
        let cancelAction = UIAlertAction(title: "No, I'm not.", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func handleAddGroup() {
        let addUsers = CreateGroupViewController()
        addUsers.nameGroup = nameGroup
        addUsers.currentUids = members
        self.navigationController?.pushViewController(addUsers, animated: true)
    }
}

extension ManageGroupViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let uid = users[indexPath.row].uid
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
                if let indexUid = selectedUsers.index(of: uid) {
                    selectedUsers.remove(at: indexUid)
                }
            } else {
                cell.accessoryType = .checkmark
                selectedUsers.append(uid)
            }
        }
        setupNavigationBar()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersCell", for: indexPath) as! UsersTableViewCell
        let user = users[indexPath.row]
        cell.nameUserLabel.text = user.name
        cell.levelLabel.text = user.level.rawValue
        let url = URL(string: user.avatar)
        cell.groupLabel.text = "Groups: \(user.groups.joined(separator: ", "))"
        cell.userAvaterView.kf.setImage(with: url!)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
