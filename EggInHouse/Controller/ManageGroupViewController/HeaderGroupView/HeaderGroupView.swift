//
//  GroupHeaderView.swift
//  EggInHouse
//
//  Created by Egg Digital on 29/1/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import UIKit

protocol HeaderGroupViewDelegate {
    func handleAddUser()
    func handleRemoveUser()
}

class HeaderGroupView: UIView {
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        fromNib()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
}

extension UIView {
    @discardableResult
    func fromNib<T: UIView>() -> T? {
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as? T else {
            return nil
        }
        self.addSubview(view)
        view.layoutAttachAll(to: self)
        return view
    }
    public func layoutAttachAll(to parentView: UIView) {
        var constraints = [NSLayoutConstraint]()
        self.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: parentView, attribute: .top, multiplier: 1.0, constant: 0))
        constraints.append(NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1.0, constant: 0))
        parentView.addConstraints(constraints)
    }
}
