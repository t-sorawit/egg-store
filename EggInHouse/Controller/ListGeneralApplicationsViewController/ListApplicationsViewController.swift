//
//  HomeApplicationsViewController.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
import CodableFirebase

class ListGeneralApplicationsViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive: Bool = false
    var currentApplication: Application!
    var filteredApps: [Application] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ListApplicationsTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ApplicationMiniCell")
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        setupNavigationItem()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    func setupNavigationItem() {
        self.navigationItem.title = "List Applications"
        if !isUserLevel() {
            let addAppItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddNewApp))
            self.navigationItem.rightBarButtonItem = addAppItem
        }
    }
    var applications: [Application] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    func fetchData() {
        Database.database().reference().child("Applications/").observe(.value) { (snapshot) in
            self.applications.removeAll()
            for children in snapshot.children {
                let child = children as? DataSnapshot
                guard let value = child?.value else { return }
                do {
                    let model = try FirebaseDecoder().decode(Application.self, from: value)
                    if isUserLevel() {
                        let isGeneral = model.group_access["General"] ?? false
                        if !isGeneral {
                            continue
                        }
                    } else {
                        let level = UserDefaults.standard.string(forKey: "level-user")
                        if level == "developer" {
                        }
                    }
                    self.applications.append(model)
                } catch let error {
                    print(error)
                }
            }
        }
    }
    @objc func handleAddNewApp() {
        let addNewAppVC = AddNewApplicationViewController()
        self.navigationController?.pushViewController(addNewAppVC, animated: true)
    }
}

extension ListGeneralApplicationsViewController: UITableViewDelegate, UITableViewDataSource, ListApplicationsViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredApps.count
        }
        return applications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApplicationMiniCell", for: indexPath) as! ListApplicationsTableViewCell
        cell.selectionStyle = .none
        cell.listApplicationsTableViewCellDelegate = self
        cell.tag = indexPath.row
        var application = self.applications[indexPath.row]
        if searchActive {
            application = self.filteredApps[indexPath.row]
        } else {
            application = self.applications[indexPath.row]
        }
        cell.nameAppLabel.text = application.name
        let iconUrl = URL(string: application.appIconUrl)
        cell.appIconView.kf.setImage(with: iconUrl)
        cell.versionLabel.text = application.version
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
            currentApplication = filteredApps[indexPath.row]
        } else {
            currentApplication = applications[indexPath.row]
        }
        let appDetailVC = AppDetailViewController()
        appDetailVC.application = currentApplication
        self.navigationController?.pushViewController(appDetailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let level = UserDefaults.standard.string(forKey: "user-level")
        if level == "user" {
            return [UITableViewRowAction]()
        } else {
            let editPermissionAction  = UITableViewRowAction(style: .default, title: "Edit Permisstion") { (_, _) in
                let editPermissionVC = InviteGroupViewController()
                editPermissionVC.isEdit = true
                editPermissionVC.nameApplication = self.applications[indexPath.row].name
                self.navigationController?.pushViewController(editPermissionVC, animated: true)
                print("editPermission")
            }
            editPermissionAction.backgroundColor = UIColor.flat.blue
            let editAppsAction = UITableViewRowAction(style: .default, title: "Edit") { (_, _) in
                print("edit")
                let editVC = AddNewApplicationViewController()
                editVC.application = self.applications[indexPath.row]
                self.navigationController?.pushViewController(editVC, animated: true)
            }
            editAppsAction.backgroundColor = UIColor.flat.yellow
            let deleteAppsAction = UITableViewRowAction(style: .default, title: "Delete") { (_, _) in
                var applicationCurrent: Application!
                if self.searchActive {
                    applicationCurrent = self.filteredApps[indexPath.row]
                } else {
                    applicationCurrent = self.applications[indexPath.row]
                }
                Firebase.deleteApplicationData(application: applicationCurrent, completion: { (hasFinished) in
                    if hasFinished {
                        self.tableView.reloadData()
                    }
                })
            }
            deleteAppsAction.backgroundColor = UIColor.flat.red
            return [deleteAppsAction, editAppsAction, editPermissionAction]
        }
    }
    func didHandleInstall(indexPath: Int) {
            let fileUrl = applications[indexPath].fileAppUrl
            let name = applications[indexPath].name
            let url = URL(string: fileUrl)
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            let refDownload = Database.database().reference().child("Applications/\(name)/download")
            refDownload.observeSingleEvent(of: .value, with: { (datasnapshot) in
                guard let downloadRaw = datasnapshot.value else { return }
                let download = downloadRaw as! Int
                refDownload.setValue(download+1)
            })
    }
}

extension ListGeneralApplicationsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredApps = searchApplication(name: searchText)
        if filteredApps.count == 0 {
            searchActive = false
        } else {
            searchActive = true
        }
        tableView.reloadData()
    }
    func searchApplication(name: String) -> [Application] {
        var filteredApps = [Application]()
        for application in applications {
            if application.name.lowercased().contains(name.lowercased()) {
                filteredApps.append(application)
            }
        }
        return filteredApps
    }
}
