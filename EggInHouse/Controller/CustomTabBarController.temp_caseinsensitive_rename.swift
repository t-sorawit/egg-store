//
//  CustomNavigationBar.swift
//  EggInHouse
//
//  Created by Egg Digital on 25/1/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit

class CustomNavigationBar: UITabBarController {

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let newSize: CGSize = CGSize(width: self.frame.size.width, height: 128)
        return newSize
    }
}
