//
//  HomeApplicationsViewController.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
import CodableFirebase
import PKHUD

class ListApplicationsViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive: Bool = false
    var currentApplication: ApplicationModel!
    var filteredApps: [ApplicationModel] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ListApplicationsTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ApplicationMiniCell")
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        tableView.tableFooterView = UIView()
        setupNavigationItem()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApplications()
        setBackgroundEmpty(applications: applications)
    }

    func setupNavigationItem() {
        self.navigationItem.title = "List Applications"
    }
    func fetchApplications() {
        Firebase.fetchApplications { (apps) in
            self.applications = apps
        }
    }
    var applications: [ApplicationModel] = [] {
        didSet {
            setBackgroundEmpty(applications: applications)
            self.tableView.reloadData()
        }
    }
    func setBackgroundEmpty(applications: [ApplicationModel]) {
        if applications.isEmpty {
            let viewController = EmptyDataView()
            tableView.backgroundView = viewController
        } else {
            tableView.backgroundView = UIView()
        }
    }
    func setBackgroundNotFound(applications: [ApplicationModel]) {
        if applications.isEmpty {
            let viewController = ResultNotFound()
            tableView.backgroundView = viewController
        } else {
            tableView.backgroundView = UIView()
        }
    }
}

extension ListApplicationsViewController: UITableViewDelegate, UITableViewDataSource, ListApplicationsViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredApps.count
        }
        return applications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApplicationMiniCell", for: indexPath) as! ListApplicationsTableViewCell
        cell.selectionStyle = .none
        cell.listApplicationsTableViewCellDelegate = self
        cell.tag = indexPath.row
        cell.indexP = indexPath
        var application = self.applications[indexPath.row]
        if searchActive {
            application = self.filteredApps[indexPath.row]
        } else {
            application = self.applications[indexPath.row]
        }
        cell.setData(applicationModel: application)
        return cell
    }
    func didHandleInstall(indexPath: IndexPath) {
        let fileUrl = applications[indexPath.row].fileAppUrl
        let name = applications[indexPath.row].appId
        let path = "itms-services://?action=download-manifest&url="
        let url = URL(string: path+fileUrl)
        if UIApplication.shared.canOpenURL(url!) {
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show()
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }

        let refDownload = Database.database().reference().child("Applications/\(name)/download")
        refDownload.observeSingleEvent(of: .value, with: { (datasnapshot) in
            guard let downloadRaw = datasnapshot.value else { return }
            let download = downloadRaw as! Int
            refDownload.setValue(download + 1)
        })
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
            currentApplication = filteredApps[indexPath.row]
        } else {
            currentApplication = applications[indexPath.row]
        }
        let appDetailVC = AppDetailViewController(name: currentApplication.appId)
        self.tabBarController?.navigationController?.pushViewController(appDetailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if isSuperAdmin() {
            let editPermissionAction  = UITableViewRowAction(style: .default, title: "Edit Permission") { (_, _) in
                let alert = UIAlertController(title: "Remove Permission", message: "Are you sure?", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                    let editPermissionVC = InviteUserViewController()
                    editPermissionVC.isEdit = true
                    var applicationCurrent: ApplicationModel!
                    if self.searchActive {
                        applicationCurrent = self.filteredApps[indexPath.row]
                    } else {
                        applicationCurrent = self.applications[indexPath.row]
                    }
                    editPermissionVC.appID = applicationCurrent.appId
                    editPermissionVC.uidPermission = applicationCurrent.permission.allKeys(forValue: true)
                    self.tabBarController?.navigationController?.pushViewController(editPermissionVC, animated: true)
                })
                let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
                alert.addAction(cancelAction)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            editPermissionAction.backgroundColor = UIColor.Flat.blue
            let editAppsAction = UITableViewRowAction(style: .default, title: "Edit") { (_, _) in
                let editVC = AddNewApplicationViewController()
                var applicationCurrent: ApplicationModel!
                if self.searchActive {
                    applicationCurrent = self.filteredApps[indexPath.row]
                } else {
                    applicationCurrent = self.applications[indexPath.row]
                }
                editVC.application = applicationCurrent
                self.tabBarController?.navigationController?.pushViewController(editVC, animated: true)
            }
            editAppsAction.backgroundColor = UIColor.Flat.yellow
            let deleteAppsAction = UITableViewRowAction(style: .default, title: "Delete") { (_, _) in
                var applicationCurrent: ApplicationModel!
                if self.searchActive {
                    applicationCurrent = self.filteredApps[indexPath.row]
                } else {
                    applicationCurrent = self.applications[indexPath.row]
                }
                let alert = UIAlertController(title: "Delete Application", message: "Are you sure?", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                    self.deleteApplication(appId: applicationCurrent.appId)
                })
                let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
            deleteAppsAction.backgroundColor = UIColor.Flat.red
            return [deleteAppsAction, editAppsAction, editPermissionAction]
        } else {
            return [UITableViewRowAction]()
        }
    }
    func deleteApplication(appId: String) {
        guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return }
        let ref = Database.database().reference().child("Applications").child(appId)
        ref.removeValue()
        Database.database().reference(withPath: "Users").child(user.uid).child("yourApp").child(appId).removeValue()
    }
}

extension ListApplicationsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredApps = searchApplication(name: searchText)
        if searchText == "" {
            searchActive = false
            setBackgroundEmpty(applications: applications)
        } else {
            searchActive = true
            setBackgroundNotFound(applications: filteredApps)
        }
        tableView.reloadData()
    }
    func searchApplication(name: String) -> [ApplicationModel] {
        var filteredApps = [ApplicationModel]()
        for application in applications {
            if application.name.lowercased().contains(name.lowercased()) {
                filteredApps.append(application)
            }
        }
        return filteredApps
    }
}
