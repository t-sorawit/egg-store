//
//  HomeApplicationsTableViewCell.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
protocol ListApplicationsViewCellDelegate: class {
    func didHandleInstall(indexPath: IndexPath)
}

class ListApplicationsTableViewCell: UITableViewCell {
    @IBOutlet weak var appIconView: UIImageView!
    @IBOutlet weak var nameAppLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var installBtn: UIButton!
    var indexP = IndexPath()
    var smth = Int()
    weak var listApplicationsTableViewCellDelegate: ListApplicationsViewCellDelegate?
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        appIconView.layer.cornerRadius = appIconView.frame.height / 4
        appIconView.clipsToBounds = true
        installBtn.setTitle("Install", for: .normal)
        installBtn.layer.cornerRadius = installBtn.frame.height / 8
        installBtn.clipsToBounds  = true
        installBtn.backgroundColor = UIColor.Yellow.egg
        installBtn.setTitleColor(.white, for: .normal)
        installBtn.addTarget(self, action: #selector(didHandleInstall), for: .touchUpInside)
    }
    func setData(applicationModel: ApplicationModel) {
        let urlIcon = URL(string: applicationModel.appIconUrl)
        appIconView.kf.setImage(with: urlIcon!)
        nameAppLabel.text = applicationModel.name
        versionLabel.text = "version: \(applicationModel.version)"
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @objc func didHandleInstall() {
        self.listApplicationsTableViewCellDelegate?.didHandleInstall(indexPath: indexP)
    }
}
