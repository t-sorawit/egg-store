//
//  LoginViewController.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import PKHUD

class LoginViewController: BaseViewController {
    @IBOutlet weak var googleSignInBtn: GIDSignInButton!
    @IBOutlet weak var facebookLoginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        view.backgroundColor = UIColor.Yellow.egg
        setupFBLoginBtn()
    }
    func setupFBLoginBtn() {
        facebookLoginBtn.backgroundColor = UIColor.Flat.facebook
        facebookLoginBtn.center = self.view.center
        facebookLoginBtn.setTitle("Login with Facebook ", for: .normal)
        facebookLoginBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        facebookLoginBtn.layer.shadowOpacity = 1.0
        facebookLoginBtn.layer.shadowOffset = CGSize(width: 0, height: 2)
        facebookLoginBtn.layer.shadowRadius = 0.0
        facebookLoginBtn.layer.masksToBounds = false
        facebookLoginBtn.layer.cornerRadius = 4.0
        facebookLoginBtn.addTarget(self, action: #selector(handleFacebookLogin), for: .touchUpInside)
    }
    @objc func handleFacebookLogin() {
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if let error = error {
                print("facebook login error\(error)")
            } else if (result?.isCancelled)! {
                print("facebook login cancelled")
            } else {
                print("faceebook logged in!")
                self.saveFbUserToFirebase()
            }
        }
    }
    func saveFbUserToFirebase() {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString
            else { return }
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        Auth.auth().signIn(with: credentials) { (user, error) in
            if let err = error {
                print("error\(err)")
                return
            }
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (_, result, error) in
                if let err = error {
                    print(err)
                    return
                }
                let userFb = result as! [String: Any]
                let uid = userFb["id"] as! String
                let name = userFb["name"] as! String
                let email = userFb["email"] as! String
                let profileUrl = "https://graph.facebook.com/\(uid)/picture?type=large&return_ssl_resources=1"
                let refUid = Database.database().reference().child("Users")
                refUid.child((user?.uid)!).observeSingleEvent(of: .value, with: { (datasnapshot) in
                    if datasnapshot.hasChildren() {
                            let user = UserModel(snapshot: datasnapshot)
                            UserDefaults.standard.setCodable(codable: user, forKey: "USER")
                        PKHUD.sharedHUD.hide(afterDelay: 1, completion: { (_) in
                            self.handleCustomTabBarController()
                        })
                    } else {
                        Firebase.addUser(uid: (user?.uid)!, name: name, email: email, profileUrl: profileUrl, level: "user", completion: { (hasFinished) in
                            if hasFinished {
                                let user = UserModel(uid: (user?.uid)!,
                                                name: name,
                                                email: email,
                                                level: "user",
                                                avatar: profileUrl,
                                                yourApp: [String]())
                                UserDefaults.standard.setCodable(codable: user, forKey: "USER")
                                PKHUD.sharedHUD.hide(afterDelay: 1, completion: { (_) in
                                  self.handleCustomTabBarController()
                                })
                            }
                        })
                    }
                })
            })
        }
    }
    func handleCustomTabBarController() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.handleUserLevel()
        appDelegate.window?.rootViewController = CustomTabBarController()
    }
}

extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        guard let idToken = user.authentication.idToken else { return }
        guard let accessToken = user.authentication.accessToken else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
        Auth.auth().signIn(with: credential) { (user, error) in
            if let err = error {
                print(err)
                return
            }
            if let uid = user?.uid, let name = user?.displayName, let email = user?.email, let profileUrl = user?.photoURL {
                Database.database().reference().child("Users/\(uid)").observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.hasChildren() {
                        let user = UserModel(snapshot: snapshot)
                        UserDefaults.standard.setCodable(codable: user, forKey: "USER")
                            PKHUD.sharedHUD.hide(afterDelay: 1, completion: { (_) in
                                self.handleCustomTabBarController()
                            })
                    } else {
                        Firebase.addUser(uid: uid, name: name, email: email, profileUrl: "\(profileUrl)", level: "user", completion: { (hasFinished) in
                            if hasFinished {
                                let user = UserModel(uid: uid,
                                                name: name,
                                                email: email,
                                                level: "user",
                                                avatar: "\(profileUrl)",
                                                yourApp: [String]())
                                UserDefaults.standard.setCodable(codable: user, forKey: "USER")
                                PKHUD.sharedHUD.hide(afterDelay: 1, completion: { (_) in
                                    self.handleCustomTabBarController()
                                })
                            }
                        })
                    }
                })
            }
        }
    }
}

extension LoginViewController: GIDSignInUIDelegate {
}
