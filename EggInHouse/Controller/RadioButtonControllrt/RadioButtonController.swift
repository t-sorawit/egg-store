//
//  RadioButtonController.swift
//  EggInHouse
//
//  Created by Kong on 2/23/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import UIKit

@objc protocol RadioButtonControllerDelegate {
    @objc func didSelectButton(selectedButton: UIButton?)
}

class RadioButtonsController: NSObject {
    fileprivate var buttonsArray = [UIButton]()
    weak var delegate: RadioButtonControllerDelegate?
    var shouldLetDeSelect = false
    init(buttons: UIButton...) {
        super.init()
        for aButton in buttons {
            aButton.addTarget(self, action: #selector(RadioButtonsController.pressed(_:)), for: UIControlEvents.touchUpInside)
        }
        self.buttonsArray = buttons
    }
    func addButton(_ aButton: UIButton) {
        buttonsArray.append(aButton)
        aButton.addTarget(self, action: #selector(RadioButtonsController.pressed(_:)), for: UIControlEvents.touchUpInside)
    }
    func removeButton(_ aButton: UIButton) {
        var iteratingButton: UIButton? = nil
        if buttonsArray.contains(aButton) {
            iteratingButton = aButton
        }
        if iteratingButton != nil {
            buttonsArray.remove(at: buttonsArray.index(of: iteratingButton!)!)
            iteratingButton!.removeTarget(self, action: #selector(RadioButtonsController.pressed(_:)), for: UIControlEvents.touchUpInside)
            iteratingButton!.isSelected = false
        }
    }
    func setButtonsArray(_ aButtonsArray: [UIButton]) {
        for aButton in aButtonsArray {
            aButton.addTarget(self, action: #selector(RadioButtonsController.pressed(_:)), for: UIControlEvents.touchUpInside)
        }
        buttonsArray = aButtonsArray
    }
    @objc func pressed(_ sender: UIButton) {
        var currentSelectedButton: UIButton? = nil
        if sender.isSelected {
            if shouldLetDeSelect {
                sender.isSelected = false
                currentSelectedButton = nil
            }
        } else {
            for aButton in buttonsArray {
                aButton.isSelected = false
            }
            sender.isSelected = true
            currentSelectedButton = sender
        }
        delegate?.didSelectButton(selectedButton: currentSelectedButton)
    }
    func selectedButton() -> UIButton? {
        guard let index = buttonsArray.index(where: { button in button.isSelected }) else { return nil }
        return buttonsArray[index]
    }
}
