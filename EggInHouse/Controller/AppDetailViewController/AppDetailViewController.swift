//
//  AppDetailViewController.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher
import PKHUD
import CodableFirebase

class AppDetailViewController: BaseViewController {
    @IBOutlet weak var appIconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var bundleIDLabel: UILabel!
    @IBOutlet weak var buildLabel: UILabel!
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var installBtn: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var descriptionTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var screenshot: UICollectionView!
    @IBOutlet weak var updateLabel: UILabel!
    var application: ApplicationModel!
    var name: String = ""
    convenience init(name: String) {
        self.init()
        self.name = name
    }
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData(name)
        setupNavigationBar()
    }
    func setupCollectionView() {
        let nibCell = UINib(nibName: "AppDetailCollectionViewCell", bundle: nil)
        self.screenshot.register(nibCell, forCellWithReuseIdentifier: "AppDetailCollectionViewCell")
        self.screenshot.delegate = self
        self.screenshot.dataSource = self
    }
    func setupView() {
        let strUrl = application.appIconUrl
        let iconUrl = URL(string: strUrl)
        appIconView.kf.setImage(with: iconUrl)
        appIconView.layer.cornerRadius = appIconView.frame.height / 4
        appIconView.clipsToBounds = true
        let dateRaw = NSDate(timeIntervalSince1970: application.lastestTimestamp / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.string(from: dateRaw as Date)
        updateLabel.text = "Update: \(date)"
        nameLabel.text = application.name
        versionLabel.text = "version: \(application.version)"
        buildLabel.text = "build: \(application.build)"
        bundleIDLabel.text = "bundle id: \(application.bundleId)"
        downloadLabel.text = "\(application.download) download"
        descriptionTextView.text = application.description
        installBtn.setTitle("Install", for: .normal)
        installBtn.layer.cornerRadius = installBtn.frame.height / 8
        installBtn.clipsToBounds  = true
        installBtn.backgroundColor = UIColor.Yellow.egg
        installBtn.setTitleColor(.white, for: .normal)
        installBtn.addTarget(self, action: #selector(handleInstall), for: .touchUpInside)
        let sizeThatFitsTextView = descriptionTextView.sizeThatFits(CGSize(width: descriptionTextView.frame.size.width, height: CGFloat(MAXFLOAT)))
        let heightOfText = sizeThatFitsTextView.height
        if heightOfText > 75 {
            descriptionTextViewHeight.constant = 75
        } else {
            descriptionTextViewHeight.constant = heightOfText
            descriptionTextView.isScrollEnabled = false
        }
    }
    func fetchData(_ name: String) {
        let ref = Database.database().reference().child("Applications").child(name)
        ref.observe(.value) { [weak self] (snapshot) in
            guard let value = snapshot.value else { return }
            do {
                let model = try FirebaseDecoder().decode(ApplicationModel.self, from: value)
                self?.application = model
                self?.setupView()
                self?.setupCollectionView()
            } catch let error {
                print(error)
            }
        }
    }
    @objc func handleInstall() {
        let fileUrl = application.fileAppUrl
        let name = application.appId
        let path = "itms-services://?action=download-manifest&url="
        let url = URL(string: path+fileUrl)
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
        let refDownload = Database.database().reference().child("Applications/\(name)/download")
        refDownload.observeSingleEvent(of: .value, with: { (datasnapshot) in
            guard let downloadRaw = datasnapshot.value else { return }
            let download = downloadRaw as! Int
            refDownload.setValue(download+1)
        })
    }
    func setupNavigationBar() {
        self.navigationItem.title = "Application Detail"
    }
}

extension AppDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let screenshotsCount = application.screenshots.count
        return screenshotsCount
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppDetailCollectionViewCell", for: indexPath) as! AppDetailCollectionViewCell
        let screenshots = application.screenshots
        let strUrl = screenshots["img\(indexPath.row+1)"]
        let url = URL(string: strUrl!)
        cell.screenshotView.kf.setImage(with: url)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageView: UIImageView = UIImageView()
        let screenshots = application.screenshots
        let strUrl = screenshots["img\(indexPath.row+1)"]
        let url = URL(string: strUrl!)
        imageView.kf.setImage(with: url!)
        imageView.frame = UIScreen.main.bounds
        imageView.backgroundColor = .black
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        imageView.addGestureRecognizer(tap)
        view.addSubview(imageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        UIApplication.shared.isStatusBarHidden = true
    }
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
        sender.view?.removeFromSuperview()
    }
}
