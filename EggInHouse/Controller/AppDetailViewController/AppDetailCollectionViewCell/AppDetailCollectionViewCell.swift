//
//  AppDetailCollectionViewCell.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit

class AppDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var screenshotView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        screenshotView.contentMode = .scaleAspectFill
    }
}
