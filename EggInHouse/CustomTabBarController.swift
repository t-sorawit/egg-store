//
//  CustomNavigationBar.swift
//  EggInHouse
//
//  Created by Egg Digital on 25/1/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Firebase

class CustomTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTabBar()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func setupTabBar() {
        guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return }
        let homeController = UINavigationController(rootViewController: HomeViewController())
        homeController.tabBarItem.image = UIImage(named: "home-tabbar")
        homeController.tabBarItem.title = "Home"
        let listOurApplicationsVC  = UINavigationController(rootViewController: ListOurApplicationsViewController())
        listOurApplicationsVC.tabBarItem.image = UIImage(named: "our-apps")
        listOurApplicationsVC.tabBarItem.title = "Our Applications"
        let listApplicationVC = UINavigationController(rootViewController: ListApplicationsViewController())
        listApplicationVC.tabBarItem.image = UIImage(named: "list-tabbar")
        listApplicationVC.tabBarItem.title = "List Applications"
        let userManagerVC = UINavigationController(rootViewController: ManagerUserViewController())
        userManagerVC.tabBarItem.image = UIImage(named: "users-tabbar")
        userManagerVC.tabBarItem.title = "User Manager"
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        switch user.level {
        case .user:
            viewControllers = [homeController, listApplicationVC]
        case .developer:
            viewControllers = [homeController, listApplicationVC, listOurApplicationsVC]
        case .superAdmin:
            viewControllers = [homeController, listApplicationVC, listOurApplicationsVC, userManagerVC]
        }
        tabBar.tintColor = UIColor.Yellow.egg
        tabBar.unselectedItemTintColor = UIColor.Flat.grayPumice
        tabBar.isTranslucent = false
        let topBorder = CALayer()
        topBorder.frame = CGRect(origin: .zero, size: CGSize(width: 1000, height: 0.5))
        topBorder.backgroundColor = UIColor(red: 0.90, green: 0.91, blue: 0.92, alpha: 1.0).cgColor
        tabBar.layer.addSublayer(topBorder)
        tabBar.clipsToBounds = true
    }
}
