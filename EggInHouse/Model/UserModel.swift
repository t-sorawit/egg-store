//
//  User.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import Firebase

struct UserModel: Codable {
    enum UserLevel: String, Codable {
        case superAdmin = "super-admin"
        case developer  = "developer"
        case user       = "user"
    }
    let uid: String
    let name: String
    let email: String
    let level: UserLevel
    let avatar: String
    let yourApp: [String]
    init?(uid: String, name: String, email: String, level: String, avatar: String, yourApp: [String]) {
        self.uid = uid
        self.name = name
        self.email = email
        self.level = UserLevel.init(rawValue: level) ?? UserLevel.user
        self.avatar = avatar
        self.yourApp = yourApp
    }
    init?(snapshot: DataSnapshot) {
        guard let dict = snapshot.value as? [String: Any] else { return nil }
        let uid = snapshot.key
        let name = dict["name"] as! String
        let email = dict["email"] as! String
        let level = dict["level"] as! String
        let avatar = dict["profileUrl"] as! String

        var appKey = [String]()
        if let apps = dict["yourApp"] as? [String: Any] {
            for aKey in apps.keys {
                appKey.append(aKey)
            }
        } else {
            appKey = [String]()
        }
        self.uid = uid
        self.name = name
        self.email = email
        self.level = UserLevel.init(rawValue: level) ?? UserLevel.user
        self.avatar = avatar
        self.yourApp = appKey
    }

}
