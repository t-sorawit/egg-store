//
//  Section.swift
//  EggInHouse
//
//  Created by Egg Digital on 19/2/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
struct SectionModel {
    var name: String
    var applications: [ApplicationModel]
    var collapsed: Bool
    init(name: String, applications: [ApplicationModel], collapsed: Bool = false) {
        self.name = name
        self.applications = applications
        self.collapsed = collapsed
    }
}
