//
//  Application.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation

struct ApplicationModel: Codable {
    let appId: String
    let name: String
    let developerID: String
    let version: String
    let build: String
    let bundleId: String
    let description: String
    let download: Int
    let appIconUrl: String
    let permission: [String: Bool]
    let screenshots: [String: String]
    let fileAppUrl: String
    let lastestTimestamp: TimeInterval
    let timeStamp: TimeInterval
    var device: Device = .iPhone
}
enum Device: String, Codable {
    case iPhone = "iPhone"
    case iPad = "iPad"
    case universal = "Universal"
}
