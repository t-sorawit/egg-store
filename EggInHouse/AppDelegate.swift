//
//  AppDelegate.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import GoogleSignIn
import IQKeyboardManagerSwift
import Alamofire
import Reachability

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        UINavigationBar.appearance().barTintColor = UIColor.Yellow.egg
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        UIApplication.shared.statusBarStyle = .lightContent
        let network: NetworkManager = NetworkManager.sharedInstance
        network.reachability.whenReachable = { reachability in
            print("connection type", reachability.connection)
            self.handleUserLevel()
        }
        network.reachability.whenUnreachable = { _ in
            print("Offline")
            self.window?.rootViewController = OfflineViewController()
        }
        return true
    }
    func handleUserLevel() {
        guard let uid = Auth.auth().currentUser?.uid else {
            window?.rootViewController = LoginViewController()
            return
        }
        let userRef = Database.database().reference(withPath: "Users").child("\(uid)/level")
        userRef.observe(DataEventType.value) { [weak self] (snapshot) in
            guard let level = snapshot.value as? String else {
                do {
                    try Auth.auth().signOut()
                    self?.window?.rootViewController = LoginViewController()
                } catch let error {
                    print("Error trying to sign out of Firebase: \(error.localizedDescription)")
                }
                return
            }
            guard let userRaw = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else {
                return
            }
            let user = UserModel(uid: userRaw.uid, name: userRaw.name, email: userRaw.email, level: level, avatar: userRaw.avatar, yourApp: userRaw.yourApp)
            UserDefaults.standard.setCodable(codable: user, forKey: "USER")
            let viewController = UINavigationController(rootViewController: CustomTabBarController())
            self?.window?.rootViewController = viewController
            self?.window?.makeKeyAndVisible()
        }
    }
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any])
        -> Bool {
            let facebookSignInHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, options: options)
            let googleSignInHandle = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
            return googleSignInHandle || facebookSignInHandle
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
