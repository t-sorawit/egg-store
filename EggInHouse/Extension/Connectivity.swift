//
//  Connectivity.swift
//  EggInHouse
//
//  Created by Kong on 18/4/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import Alamofire
class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
