//
//  UIStoryboard.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard {
    static func getViewController(storyboardName: String, viewControllerId: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerId)
        return viewController
    }
}
