//
//  Dictionary.swift
//  EggInHouse
//
//  Created by Kong on 3/21/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation

extension Dictionary where Value: Equatable {
    func allKeys(forValue val: Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.key }
    }
}
