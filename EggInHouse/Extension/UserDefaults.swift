//
//  UserDefault.swift
//  EggInHouse
//
//  Created by Kong on 3/2/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation

extension UserDefaults {
    func setCodable<T: Encodable>(codable: T, forKey key: String) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(codable)
            let jsonString = String(data: data, encoding: .utf8)!
            print("Saving \(key): \(jsonString)")
            self.set(jsonString, forKey: key)
        } catch {
            print("Saving \(key) failed: \(error)")
        }
    }
    func getCodable<T: Decodable>(_ codable: T.Type, forKey key: String) -> T? {
        guard let jsonString = self.string(forKey: key) else { return nil }
        guard let data = jsonString.data(using: .utf8) else { return nil }
        let json = try? JSONSerialization.jsonObject(with: data, options: [])
        print("Loading \(key): \(json ?? "")")
        let decoder = JSONDecoder()
        return try? decoder.decode(codable, from: data)
    }
}
