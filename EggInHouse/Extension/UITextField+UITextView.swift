//
//  UITextField.swift
//  EggInHouse
//
//  Created by Egg Digital on 1/2/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func setBottomBorder(width: CGFloat, color: CGColor) {
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        topBorder.backgroundColor = color
        self.layer.addSublayer(topBorder)
        self.clipsToBounds = true
    }
}
