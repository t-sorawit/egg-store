//
//  Helper.swift
//  EggInHouse
//
//  Created by Egg Digital on 15/2/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation

func isUserLevel() -> Bool {
    guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return true }
    if user.level == .user {
        return true
    }
    return false
}
func isSuperAdmin() -> Bool {
    guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return false }
    if user.level == .superAdmin {
        return true
    } else {
        return false
    }
}
