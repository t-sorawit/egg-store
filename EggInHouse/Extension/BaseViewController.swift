//
//  BaseViewController.swift
//  EggInHouse
//
//  Created by Egg Digital on 1/2/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationItem.backBarButtonItem = backButton
        self.tabBarController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationWillResignActive(notification:)),
            name: NSNotification.Name.UIApplicationWillResignActive,
            object: nil)
    }
    @objc func applicationWillResignActive(notification: NSNotification) {
        PKHUD.sharedHUD.hide()
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        print(String(describing: type(of: self)) + ": deinit")
    }
    func showAlertMsg(withViewController viewController: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    func showError(title: String, subtitle: String, delay: TimeInterval, completion: @escaping(Bool) -> Void) {
        PKHUD.sharedHUD.contentView = PKHUDErrorView(title: title, subtitle: subtitle)
        PKHUD.sharedHUD.userInteractionOnUnderlyingViewsEnabled = true
        PKHUD.sharedHUD.hide(afterDelay: 3) { (finish) in
            completion(finish)
        }
    }
}
