//
//  UIColor.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct Yellow {
        static let egg = UIColor(red: 0.98, green: 0.73, blue: 0.18, alpha: 1.0)
        static let dark = UIColor(red: 0.90, green: 0.60, blue: 0.00, alpha: 1.0)
    }
    struct Flat {
        static let red = UIColor(red: 0.91, green: 0.30, blue: 0.24, alpha: 1.0)
        static let blue = UIColor(red: 0.16, green: 0.50, blue: 0.73, alpha: 1.0)
        static let yellow = UIColor(red: 0.95, green: 0.77, blue: 0.06, alpha: 1.0)
        static let green = UIColor(red: 0.15, green: 0.68, blue: 0.38, alpha: 1.0)
        static let midnightblue = UIColor(red: 0.17, green: 0.24, blue: 0.31, alpha: 1.0)
        static let grayPumice = UIColor(red: 0.82, green: 0.84, blue: 0.83, alpha: 1.0)
        static let facebook = UIColor(red: 0.23, green: 0.35, blue: 0.60, alpha: 1.0)
        static let grayBrown = UIColor(red: 0.13, green: 0.13, blue: 0.13, alpha: 1.0)
        static let grayLight = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1.0)
        static let orange = UIColor(red: 0.98, green: 0.41, blue: 0.05, alpha: 1.0)
    }
}
