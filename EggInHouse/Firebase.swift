//
//  Firebase.swift
//  EggInHousesDemo
//
//  Created by Egg Digital on 1/22/2561 BE.
//  Copyright © 2561 Egg Digital. All rights reserved.
//

import Foundation
import CodableFirebase
import Firebase

struct Firebase {
    static let refApplications = Database.database().reference().child("Applications")
    static let refUsers = Database.database().reference().child("Users")
    static let refGroups = Database.database().reference().child("Groups")
    static func getCurrentUid() -> String {
        if let uid = Auth.auth().currentUser?.uid {
            return uid
        } else {
            return "uid"
        }
    }
    static func fetchApplications(completion: @escaping ([ApplicationModel]) -> Void) {
        guard let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER") else { return }
        let applicationRef = Database.database().reference(withPath: "Applications")
        applicationRef.observe(.value) { (snapshot) in
            var applicationModelItem: [ApplicationModel] = []
            if snapshot.childrenCount == 0 {
                completion([ApplicationModel]())
            }
            for children in snapshot.children {
                let child = children as? DataSnapshot
                guard let value = child?.value else { return }
                do {
                    let model = try FirebaseDecoder().decode(ApplicationModel.self, from: value)
                    switch user.level {
                    case .superAdmin :
                            applicationModelItem.append(model)
                            completion(applicationModelItem)
                    case .developer, .user:
                        let modelDevice = UIDevice.current.model
                        let appPermission = model.permission[user.uid] ?? false
                        let generalApp = model.permission["General"] ?? false
                        let ourApp = model.developerID.contains(user.uid)
                        if generalApp || appPermission || ourApp {
                            if modelDevice == "iPad" {
                                applicationModelItem.append(model)
                                completion(applicationModelItem)
                            } else if modelDevice == model.device.rawValue {
                                applicationModelItem.append(model)
                                completion(applicationModelItem)
                            } else {
                                completion(applicationModelItem)
                            }
                        }
                    }
                } catch let error {
                    print("APPLICATION ERROR", error)
                }
            }
        }
    }
    static func fetchOurApplications(uid: String, completion: @escaping ([ApplicationModel]) -> Void) {
        let applicationRef = Database.database().reference(withPath: "Applications")
        Database.database().reference(withPath: "Users").child(uid).observe(.value) { (snapshot) in
            guard let value = snapshot.value else {
                return
            }
            var applicationList: [ApplicationModel] = []
            let user = value as! [String: Any]
            guard let appIDsDict = user["yourApp"] as? [String: Any] else {
                completion([ApplicationModel]())
                return
            }
            let appIDs = Array(appIDsDict.keys)
            for appID in appIDs {
                applicationRef.child(appID).observeSingleEvent(of: .value, with: { (snapshot) in
                    guard let value = snapshot.value else {
                        return
                    }
                    do {
                        let model = try FirebaseDecoder().decode(ApplicationModel.self, from: value)
                        applicationList.append(model)
                        completion(applicationList)
                    } catch let error {
                        print(error)
                    }
                })
            }

        }
    }
    static func setGroupUser(nameGroup: String, member: [String], completion: @escaping (Bool) -> Void) {
        let value = ["name": nameGroup, "members": member[0]]
        refGroups.childByAutoId().setValue(value) { (error, _) in
            if let error = error {
                print(error)
                return
            } else {
                completion(true)
            }
        }
    }
    static func users(completion: @escaping ([UserModel]) -> Void) {
        let ref = Database.database().reference().child("Users")
        ref.observe(.value, with: { (snapshot) in
            guard let snapshot = snapshot.children.allObjects as? [DataSnapshot] else {
                return completion([])
            }
            let users = snapshot.reversed().compactMap(UserModel.init)

            completion(users)
        })
    }
    static func addUser(uid: String, name: String, email: String, profileUrl: String, level: String, completion: @escaping (Bool) -> Void) {
        let usersRef = Database.database().reference().child("Users")
        usersRef.child(uid).setValue(["name": name, "email": email, "profileUrl": profileUrl, "level": "user"]) { (err, _) in
            if let error = err {
                print(error)
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    static func loggedIn() -> Bool {
        if Auth.auth().currentUser != nil {
            return true
        } else {
            return false
        }
    }
    static func deleteApplicationData(application: ApplicationModel, completion: @escaping (_ hasFinished: Bool) -> Void ) {
        let name = application.name
        refApplications.child(name).removeValue { (error, ref) in
            if let error = error {
                print(error)
            } else {
                print("delete complete: \(ref.key)")
                completion(true)
            }
        }
    }
    static func getApplications(byChild child: String, completion: @escaping (_ result: [ApplicationModel]) -> Void ) {
        var applications = [ApplicationModel]()
        refApplications.queryOrdered(byChild: child).queryLimited(toFirst: UInt(10)).observe(.value, with: { (snap) in
            applications = [ApplicationModel]()
            for children in snap.children {
                let child = children as? DataSnapshot
                guard let value = child?.value else { return }
                do {
                    let user = UserDefaults.standard.getCodable(UserModel.self, forKey: "USER")
                    if user?.level == .developer {
                    }
                    let app = try FirebaseDecoder().decode(ApplicationModel.self, from: value)
                    let modelDevice = UIDevice.current.model
                    let appDevice = app.device.rawValue
                    if modelDevice == "iPad" {
                        if user?.level == .superAdmin {
                            applications.append(app)
                        } else {
                            if app.permission["General"] ?? false {
                                applications.append(app)
                            }
                        }
                    } else if modelDevice == appDevice {
                        if user?.level == .superAdmin {
                            applications.append(app)
                        } else {
                            if app.permission["General"] ?? false {
                                applications.append(app)
                            }
                        }
                    }
                } catch let error {
                    print(error)
                }
            }
            completion(applications.reversed())
        })
    }
    static func getCurrentUser(uid: String, completion: @escaping (UserModel) -> Void) {
        refUsers.child(uid).observeSingleEvent(of: .value) { (datasnapshot) in
            let user = UserModel(snapshot: datasnapshot)
            completion(user!)
        }
    }
    static func uploadImageFile(appName: String, appIcon: UIImage, screenshots: [UIImage], completion: @escaping (_ hasFinished: Bool, _ iconUrl: String, _ screenshotsUrl: [String]) -> Void) {
        var screenshotsUrl = [String]()
        var screenshotCount = 0
        for screenshot in screenshots {
            let refStorage = Storage.storage().reference().child("\(appName)")
            let regStorageScreenshot = refStorage.child("screenshots").child("\(screenshotCount).png")
            let refStorageAppIcon = refStorage.child("appIcon").child("appIcon.png")
            guard let imageData = UIImagePNGRepresentation(screenshot) else {
                return
            }
            guard let iconData = UIImagePNGRepresentation(appIcon) else {
                return
            }
            //upload app icon
            var iconUrl: String!
            refStorageAppIcon.putData(iconData, metadata: nil, completion: { (metaData, error) in
                if let error = error {
                    print(error)
                    return
                }
                if let url = metaData?.downloadURL()?.absoluteString {
                    iconUrl = url
                    //upload screenshot
                    regStorageScreenshot.putData(imageData, metadata: nil, completion: { (metaData, error) in
                        if let error = error {
                            print(error)
                            return
                        }
                        if let imageUrl = metaData?.downloadURL()?.absoluteString {
                            screenshotsUrl.append(imageUrl)
                            screenshotCount += 1
                            if screenshotCount == screenshots.count {
                                completion(true, iconUrl, screenshotsUrl)
                            }
                        }
                    })
                }
            })
        }
    }
}
